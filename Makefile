PACKAGE_NAME = glashctl
PACKAGE_VERSION = 0.4.4
PKG_DEPS = gtkmm-2.4>=2.8.8 lash-1.0>=0.5.1 jack>=0.100.0

DATA = lash_96px.png tinykeyboard.png tinywave.png
DOCS = AUTHORS COPYING README ChangeLog

ARCHIVES = libglashctl.a
libglashctl_a_SOURCES = \
	jackwrapper.cpp jackwrapper.hpp \
	lashserverinterface.cpp lashserverinterface.hpp \
	lashwrapper.cpp lashwrapper.hpp \
	popupmenu.cpp popupmenu.hpp \
	sessionchooserdialog.cpp sessionchooserdialog.hpp
libglashctl_a_CFLAGS = `pkg-config --cflags gtkmm-2.4 lash-1.0 jack`
popupmenu_cpp_CFLAGS = -DDATA_DIR=\"$(pkgdatadir)\"

PROGRAMS = glashctl wmglashctl
glashctl_SOURCES = \
	main.cpp \
	trayicon.cpp trayicon.hpp
trayicon_cpp_CFLAGS = -DDATA_DIR=\"$(pkgdatadir)\"
glashctl_CFLAGS = `pkg-config --cflags gtkmm-2.4 lash-1.0 jack`
glashctl_LDFLAGS = `pkg-config --libs gtkmm-2.4 lash-1.0 jack` libglashctl.a

wmglashctl_SOURCES = \
	lash_active.xpm \
	lash_inactive.xpm \
	wmmain.cpp \
	wmdockicon.cpp wmdockicon.hpp
wmglashctl_CFLAGS = `pkg-config --cflags gtkmm-2.4 lash-1.0 jack`
wmglashctl_LDFLAGS = `pkg-config --libs gtkmm-2.4 lash-1.0 jack` libglashctl.a


include Makefile.template
