/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#ifndef LASHWRAPPER_HPP
#define LASHWRAPPER_HPP

#include <string>

#include <sigc++/sigc++.h>

#include "lashserverinterface.hpp"


/** This class wraps all LASH functionality used by this applet. */
class LASHWrapper : public sigc::trackable {
public:
  
  LASHWrapper(int* argc, char*** argv);

  void start_lashd();
  void stop_lashd();
  
  void restore_session(const std::string& dir);
  void save_session();
  void set_session_dir(const std::string& dir);
  void set_session_name(const std::string& name);
  void close_session();
  
  sigc::signal<void> signal_lashd_started;
  sigc::signal<void> signal_lashd_stopped;
  sigc::signal<void, const std::string&> signal_session_changed;
  sigc::signal<void, const std::string&, const std::string&> 
  signal_session_saved;
  sigc::signal<void, const std::string&> signal_event_received;
  
private:
  
  void wait_for_sessions(const std::string& session);
  void really_stop_lashd();
  
  LASHServerInterface* create_server_interface(int timeout);
  bool check_running();
  
  int m_running;
  int m_old_running;
  bool m_send_delayed_start;
  LASHServerInterface* m_lsi;
  lash_args_t* m_args;
};


#endif
