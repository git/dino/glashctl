// -*- Mode: C++ ; c-basic-offset: 2 -*-
/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006 Nedko Arnaudov <nedko@arnaudov.name>
   Modified by Lars Luthman (blame all bugs on me!)
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 3 of the License.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <cstring>
#include <iostream>

#include <gtk/gtkmain.h>

#include "wmdockicon.hpp"

#include <gdk/gdkx.h>

#include "lash_active.xpm"
#include "lash_inactive.xpm"


using namespace Gdk;
using namespace Glib;
using namespace sigc;
using namespace std;


WMDockIcon::WMDockIcon() {
  
  // Set initial Gdk::Window attributes
  GdkWindowAttr attr;
  GdkWindowAttr attri;
  memset(&attr, 0, sizeof(GdkWindowAttr));
  attr.width = 64;
  attr.height = 64;
  attr.title = strdup("wmglashctl");
  attr.event_mask =
    GDK_BUTTON_PRESS_MASK |
    GDK_EXPOSURE_MASK |
    GDK_VISIBILITY_NOTIFY_MASK;
  attr.wclass = GDK_INPUT_OUTPUT;
  attr.visual = gdk_visual_get_system();
  attr.colormap = gdk_colormap_get_system();
  attr.wmclass_name = strdup("wmglashctl");
  attr.wmclass_class = strdup("wmglashctl");
  attr.window_type = GDK_WINDOW_TOPLEVEL;

  // Make a copy for the iconwin - parameters are the same
  memcpy(&attri, &attr, sizeof(GdkWindowAttr));
  attri.window_type = GDK_WINDOW_CHILD;
  
  // Create dummy main window
  // XXX why can't I create a Gdk::Window without a parent window?
  if (!(m_win = wrap((GdkWindowObject*)gdk_window_new(0, &attr, 
                                                      GDK_WA_TITLE | 
                                                      GDK_WA_VISUAL | 
                                                      GDK_WA_WMCLASS |
                                                      GDK_WA_COLORMAP)))) {
    cerr<<"FATAL: Cannot make toplevel window"<<endl;
    exit(1);
  }
  
  // Create icon window
  if (!(m_iconwin = Gdk::Window::create(m_win, &attri, GDK_WA_TITLE | 
                                        GDK_WA_WMCLASS))) {
    cerr<<"FATAL: Cannot make icon window"<<endl;
    exit(1);
  }  
  
  // This X hackery is needed to make the dockapp work in Fluxbox
  XSizeHints sizehints;
  sizehints.flags = USSize;
  sizehints.width = 64;
  sizehints.height = 64;
  ::Window win = GDK_WINDOW_XWINDOW(m_win->gobj());
  XSetWMNormalHints(GDK_WINDOW_XDISPLAY(m_win->gobj()), win, &sizehints);
  
  // Create background pixmaps and set the window shape
  Glib::RefPtr<Gdk::Bitmap> mask;
  m_active_pixmap = Gdk::Pixmap::create_from_xpm(Gdk::Colormap::get_system(), 
                                                 mask, Color(),
                                                 lash_active_xpm);
  m_inactive_pixmap = Gdk::Pixmap::create_from_xpm(Gdk::Colormap::get_system(),
                                                   mask, Color(),
                                                   lash_inactive_xpm);
  m_win->shape_combine_mask(mask, 0, 0);
  m_iconwin->shape_combine_mask(mask, 0, 0);
  m_win->set_back_pixmap(m_inactive_pixmap, false);
  m_iconwin->set_back_pixmap(m_inactive_pixmap, false);
  
  // Set some window hints
  m_win->set_decorations(WMDecoration(0));
  m_win->set_skip_taskbar_hint(true);
  m_win->set_icon(m_iconwin, RefPtr<Gdk::Pixmap>(0), RefPtr<Bitmap>(0));
  m_win->set_group(m_win);
  m_win->show();
  
  // This X hackery is also needed to make the dockapp work in Fluxbox
  XWMHints wmhints;
  ::Window iconwin = GDK_WINDOW_XWINDOW(m_iconwin->gobj());
  wmhints.initial_state = WithdrawnState;
  wmhints.icon_window = iconwin;
  wmhints.icon_x = 0;
  wmhints.icon_y = 0;
  wmhints.window_group = win;
  wmhints.flags = (StateHint | IconWindowHint | 
                   IconPositionHint | WindowGroupHint);
  XSetWMHints(GDK_WINDOW_XDISPLAY(m_win->gobj()), win, &wmhints);
  
  // We can't connect directly to a Gdk::Window, so we need to filter the
  // events before they reach GTK
  gdk_event_handler_set(&WMDockIcon::button_pressed, this, 0);
  
  // Free some memory. Dirty C!
  free(attr.title);
  free(attr.wmclass_name);
  free(attr.wmclass_class);
}


void WMDockIcon::lashd_started() {
  m_iconwin->set_back_pixmap(m_active_pixmap, false);
  m_iconwin->clear();
  m_win->set_back_pixmap(m_active_pixmap, false);
  m_win->clear();
}


void WMDockIcon::lashd_stopped() {
  m_iconwin->set_back_pixmap(m_inactive_pixmap, false);
  m_iconwin->clear();
  m_win->set_back_pixmap(m_inactive_pixmap, false);
  m_win->clear();
}


void WMDockIcon::button_pressed(GdkEvent* event, gpointer data) {
  WMDockIcon* me = static_cast<WMDockIcon*>(data);
  if (event->type == GDK_BUTTON_PRESS && 
      event->button.button == 3) {
    me->signal_popup(event->button.button, event->button.time);
    return;
  }

  // This is needed for some obscure reason.
  if (event->type == GDK_EXPOSE || event->type == GDK_VISIBILITY_NOTIFY)
    me->m_win->clear();

  gtk_main_do_event(event);
}
