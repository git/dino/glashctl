/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <algorithm>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>

#include <gtkmm/stock.h>
#include <gtkmm/main.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/entry.h>
#include <gtk/gtksignal.h>

#include <sigc++/sigc++.h>

#include <jack/types.h>

#include "trayicon.hpp"


using namespace Gdk;
using namespace Glib;
using namespace Gtk;
using namespace sigc;
using namespace std;


TrayIcon::TrayIcon() 
  : m_active_pixbuf(Pixbuf::create_from_file(DATA_DIR "/lash_96px.png")) {
  
  m_inactive_pixbuf = m_active_pixbuf->copy();
  
  // create an "insensitive" version of the icon
  int n_channels = m_inactive_pixbuf->get_n_channels();
  if (m_inactive_pixbuf->get_colorspace() == COLORSPACE_RGB &&
      m_inactive_pixbuf->get_bits_per_sample() == 8 &&
      m_inactive_pixbuf->get_has_alpha() &&
      n_channels == 4) {
    
    int width = m_inactive_pixbuf->get_width();
    int height = m_inactive_pixbuf->get_height();
    int rowstride = m_inactive_pixbuf->get_rowstride();
    guint8* pixels = m_inactive_pixbuf->get_pixels();
    
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        guint8* p = pixels + y * rowstride + x * n_channels;
        guint8& red = p[0];
        guint8& green = p[1];
        guint8& blue = p[2];
        guint8& alpha = p[3];
        guint8 average = guint8((float(red) + float(green) + float(blue)) / 3);
        red = average;
        green = average;
        blue = average;
        alpha /= 2;
      }
    }
  }
  
  set(m_inactive_pixbuf);
  set_tooltip("Linux Audio Session Handler");
  
  // we need to use the C API here since the signal isn't wrapped (gtkmm 2.10.1)
  g_signal_connect(G_OBJECT(gobj()), "popup-menu",
                   GTK_SIGNAL_FUNC(&TrayIcon::popup_menu), (void*)this);
}


void TrayIcon::lashd_started() {
  set(m_active_pixbuf);
}


void TrayIcon::lashd_stopped() {
  set(m_inactive_pixbuf);
}


void TrayIcon::popup_menu(GtkStatusIcon* obj, guint button, 
                          guint activate_time, gpointer data) {
  static_cast<TrayIcon*>(data)->signal_popup(button, activate_time);
}


