/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <cstring>
#include <iostream>
#include <sstream>
#include <unistd.h>

#include <glibmm.h>
#include <sigc++/sigc++.h>

#include "lashserverinterface.hpp"


using namespace Glib;
using namespace sigc;
using namespace std;


LASHServerInterface::LASHServerInterface(lash_args_t* args,
                                         const string& classname,
                                         int timeout) : m_client(NULL) {
  char* dummy_arg = strdup("thisisnotarealprogramname_q238dnvsd9824fj");
  char** dummy_argv = &dummy_arg;
  int dummy_argc = 1;
  time_t now = time(NULL);
  do {
    m_client = lash_init(lash_extract_args(&dummy_argc, &dummy_argv), 
                         classname.c_str(), LASH_Server_Interface, 
                         LASH_PROTOCOL(2, 0));
    if (m_client || time(NULL) >= now + timeout)
      break;
    ::usleep(500000);
  } while (time(NULL) < now + timeout);
  
  m_connection = signal_timeout().
    connect(mem_fun(*this, &LASHServerInterface::check_events), 200);
}


LASHServerInterface::~LASHServerInterface() {
  m_connection.disconnect();
}


bool LASHServerInterface::is_valid() const {
  return lash_enabled(m_client);
}


bool LASHServerInterface::has_session() const {
  return m_projects.size() > 0;
}

  
void LASHServerInterface::restore_session(const string& directory) {
  if (is_valid()) {
    lash_event_t* event = lash_event_new_with_type(LASH_Project_Add);
    lash_event_set_string(event, directory.c_str());
    lash_send_event(m_client, event);
  }
}


void LASHServerInterface::set_session_dir(const string& directory) {
  if (is_valid() && has_session()) {
    lash_event_t* event = lash_event_new_with_type(LASH_Project_Dir);
    lash_event_set_string(event, directory.c_str());
    lash_event_set_project(event, m_projects.begin()->name.c_str());
    lash_send_event(m_client, event);
  }
}


void LASHServerInterface::save_session() {
  if (is_valid() && has_session()) {
    lash_event_t* event = lash_event_new_with_type(LASH_Save);
    lash_event_set_project(event, m_projects.begin()->name.c_str());
    lash_send_event(m_client, event);
  }
}


void LASHServerInterface::close_session() {
  if (is_valid() && has_session()) {
    lash_event_t* event = lash_event_new_with_type(LASH_Project_Remove);
    lash_event_set_project(event, m_projects.begin()->name.c_str());
    lash_send_event(m_client, event);
  }
}


void LASHServerInterface::close_all_sessions() {
  if (is_valid()) {
    list<Project>::iterator iter;
    for (iter = m_projects.begin(); iter != m_projects.end(); ++iter) {
      lash_event_t* event = lash_event_new_with_type(LASH_Project_Remove);
      lash_event_set_project(event, m_projects.begin()->name.c_str());
      lash_send_event(m_client, event);
    }
  }
}


void LASHServerInterface::rename_session(const string& new_name) {
  if (is_valid() && has_session()) {
    lash_event_t* event = lash_event_new_with_type(LASH_Project_Name);
    lash_event_set_project(event, m_projects.begin()->name.c_str());
    lash_event_set_string(event, new_name.c_str());
    lash_send_event(m_client, event);
  }
}


bool LASHServerInterface::check_events() {
  
  if (!is_valid())
    return true;
  
  lash_event_t* event;
  while ((event = lash_get_event(m_client))) {
    
    switch (lash_event_get_type(event)) {
      
    case LASH_Project_Add: {
      Project p = { lash_event_get_string(event), "" };
      m_projects.push_front(p);
      ostringstream oss;
      oss<<"Project '"<<lash_event_get_string(event)<<"' added";
      signal_event_received(oss.str());
      signal_session_changed(lash_event_get_string(event));
      break;
    }
      
    case LASH_Project_Remove: {
      list<Project>::iterator iter;
      for (iter = m_projects.begin(); iter != m_projects.end(); ++iter) {
        if (iter->name == lash_event_get_project(event)) {
          bool active = false;
          if (iter == m_projects.begin())
            active = true;
          m_projects.erase(iter);
          ostringstream oss;
          oss<<"Project '"<<lash_event_get_project(event)<<"' removed";
          signal_event_received(oss.str());
          if (has_session())
            signal_session_changed(m_projects.begin()->name);
          else
            signal_session_changed("");
          break;
        }
      }
      break;
    }
      
    case LASH_Project_Dir: {
      list<Project>::iterator iter;
      for (iter = m_projects.begin(); iter != m_projects.end(); ++iter) {
        if (iter->name == lash_event_get_project(event)) {
          iter->dir = lash_event_get_string(event);
          ostringstream oss;
          oss<<"Project '"<<lash_event_get_project(event)
             <<"' has been moved to '"<<lash_event_get_string(event)<<"'";
          signal_event_received(oss.str());
          break;
        }
      }
      break;
    }
      
    case LASH_Project_Name: {      
      list<Project>::iterator iter;
      for (iter = m_projects.begin(); iter != m_projects.end(); ++iter) {
        if (iter->name == lash_event_get_project(event)) {
          iter->name = lash_event_get_string(event);
          ostringstream oss;
          oss<<"Project '"<<lash_event_get_project(event)
             <<"' has changed name to '"<<lash_event_get_string(event)<<"'";
          signal_event_received(oss.str());
          if (iter == m_projects.begin())
            signal_session_changed(iter->name);
          break;
        }
      }
      break;
    }
      
    case LASH_Percentage: {
      ostringstream oss;
      oss<<"LASH_Percentage event: "<<lash_event_get_project(event)<<" "
         <<lash_event_get_string(event);
      signal_event_received(oss.str());
      if (string("100") == lash_event_get_string(event)) {
        cerr<<"Project '"<<lash_event_get_project(event)<<"' saved"<<endl;
        list<Project>::const_iterator iter;
        for (iter = m_projects.begin(); iter != m_projects.end(); ++iter) {
          if (iter->name == lash_event_get_project(event)) {
            signal_session_saved(iter->name, iter->dir);
            break;
          }
        }
      }
      break;
    }
      
    default:
      ostringstream oss;
      oss<<"Got unhandled LASH event from the server";
      signal_event_received(oss.str());
      cerr<<oss.str()<<endl;
    }
    
    lash_event_destroy(event);
  }
  
  return true;
}
