/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <algorithm>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>

#include <gtkmm/stock.h>
#include <gtkmm/main.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/entry.h>
#include <gtk/gtksignal.h>

#include <sigc++/sigc++.h>

#include <jack/types.h>

#include "popupmenu.hpp"


using namespace Gdk;
using namespace Glib;
using namespace Gtk;
using namespace sigc;
using namespace std;


PopupMenu::PopupMenu() 
  : m_midi_pixbuf(Pixbuf::create_from_file(DATA_DIR "/tinykeyboard.png")),
    m_audio_pixbuf(Pixbuf::create_from_file(DATA_DIR "/tinywave.png")),
    m_connectitem("Connect"),
    m_startitem("Start session handler"),
    m_stopitem("Stop session handler"),
    m_restoreitem("Restore session"),
    m_restorerecentitem("Restore recent"),
    m_saveitem("Save session"),
    m_closeitem("Close session"),
    m_setsessiondiritem("Move session directory"),
    m_setsessionnameitem("Rename session"),
    m_session_save_dialog("Enter directory name", FILE_CHOOSER_ACTION_SAVE),
    m_lashd_running(false) {
  
  // load the recent sessions
  ifstream ifs((Glib::getenv("HOME") + "/.glashctl_recent").c_str());
  while (ifs.good()) {
    string name, dir;
    getline(ifs, name);
    getline(ifs, dir);
    if (name != "") {
      m_recent_sessions.push_back(make_pair(name, dir));
      MenuItem* mi = manage(new MenuItem(name));
      mi->signal_activate().connect(bind(signal_restore_session, dir));
      m_restorerecentmenu.append(*mi);
    }
  }
  ifs.close();
  m_restorerecentmenu.show_all();
  
  m_session_dialog.add_button(Stock::OK, RESPONSE_OK);
  m_session_dialog.add_button(Stock::CANCEL, RESPONSE_CANCEL);
  m_session_dialog.set_default_response(RESPONSE_OK);
  
  m_session_save_dialog.add_button(Stock::OK, RESPONSE_OK);
  m_session_save_dialog.add_button(Stock::CANCEL, RESPONSE_CANCEL);
  m_session_save_dialog.set_default_response(RESPONSE_OK);
  
  append(m_connectitem);
  m_connectitem.set_submenu(m_connect_menu);
  append(*manage(new SeparatorMenuItem));
  
  append(m_restoreitem);
  m_restoreitem.set_sensitive(false);
  m_restoreitem.signal_activate().connect(mem_fun(*this, 
                                                  &PopupMenu::do_restore));
  append(m_restorerecentitem);
  m_restorerecentitem.set_sensitive(false);
  m_restorerecentitem.set_submenu(m_restorerecentmenu);
  
  append(m_setsessionnameitem);
  m_setsessionnameitem.set_sensitive(false);
  m_setsessionnameitem.signal_activate().
    connect(mem_fun(*this, &PopupMenu::do_set_session_name));
  
  append(m_saveitem);
  m_saveitem.set_sensitive(false);
  m_saveitem.signal_activate().connect(signal_save_session);

  append(m_setsessiondiritem);
  m_setsessiondiritem.set_sensitive(false);
  m_setsessiondiritem.signal_activate().
    connect(mem_fun(*this, &PopupMenu::do_set_session_dir));

  append(m_closeitem);
  m_closeitem.set_sensitive(false);
  m_closeitem.signal_activate().connect(signal_close_session);
  append(*manage(new SeparatorMenuItem));

  m_startitem.signal_activate().connect(signal_start_lashd);
  append(m_startitem);
  m_stopitem.signal_activate().connect(signal_stop_lashd);
  m_stopitem.set_sensitive(false);
  append(m_stopitem);
  append(*manage(new SeparatorMenuItem));
  
  MenuItem* quititem = manage(new MenuItem("Quit"));
  quititem->signal_activate().connect(mem_fun(*this, &PopupMenu::do_quit));
  append(*quititem);
  
  show_all();
  
}


void PopupMenu::lashd_started() {
  m_startitem.set_sensitive(false);
  m_stopitem.set_sensitive(true);
  m_restoreitem.set_sensitive(true);
  m_restorerecentitem.set_sensitive(true);
  m_lashd_running = true;
}


void PopupMenu::lashd_stopped() {
  m_stopitem.set_sensitive(false);
  m_startitem.set_sensitive(true);
  m_restoreitem.set_sensitive(false);
  m_restorerecentitem.set_sensitive(false);
  m_saveitem.set_sensitive(false);
  m_setsessiondiritem.set_sensitive(false);
  m_setsessionnameitem.set_sensitive(false);
  m_closeitem.set_sensitive(false);
  dynamic_cast<Label*>(m_saveitem.get_child())->set_text("Save session");
  dynamic_cast<Label*>(m_setsessiondiritem.get_child())->
    set_text("Move session directory");
  dynamic_cast<Label*>(m_closeitem.get_child())->set_text("Close session");
  dynamic_cast<Label*>(m_setsessionnameitem.get_child())->
    set_text("Rename session");
  m_lashd_running = false;
  internal_signal_lashd_stopped();
}


void PopupMenu::session_changed(const string& name) {
  m_session_name = name;
  if (name == "") {
    m_saveitem.set_sensitive(false);
    m_setsessiondiritem.set_sensitive(false);
    m_setsessionnameitem.set_sensitive(false);
    m_closeitem.set_sensitive(false);
    m_stopitem.set_sensitive(true);
    m_restoreitem.set_sensitive(true);
    m_restorerecentitem.set_sensitive(true);
    dynamic_cast<Label*>(m_saveitem.get_child())->set_text("Save session");
    dynamic_cast<Label*>(m_closeitem.get_child())->set_text("Close session");
    dynamic_cast<Label*>(m_setsessiondiritem.get_child())->
      set_text("Move session directory");
    dynamic_cast<Label*>(m_setsessionnameitem.get_child())->
      set_text("Rename session");
  }
  else {
    dynamic_cast<Label*>(m_saveitem.get_child())->
      set_text(string("Save '") + name + "'");
    dynamic_cast<Label*>(m_closeitem.get_child())->
      set_text(string("Close '") + name + "'");
    dynamic_cast<Label*>(m_setsessiondiritem.get_child())->
      set_text(string("Move '") + name + "' directory");
    dynamic_cast<Label*>(m_setsessionnameitem.get_child())->
      set_text(string("Rename '") + name + "'");
    m_saveitem.set_sensitive(true);
    m_setsessiondiritem.set_sensitive(true);
    m_setsessionnameitem.set_sensitive(true);
    m_closeitem.set_sensitive(true);
    m_stopitem.set_sensitive(false);
    m_restoreitem.set_sensitive(false);
    m_restorerecentitem.set_sensitive(false);
  }
}


void PopupMenu::session_saved(const std::string& name, const std::string& dir) {

  vector<pair<string, string> >::iterator iter;
  bool old = false;
  for (iter = m_recent_sessions.begin(); 
       iter != m_recent_sessions.end(); ++iter) {
    if (iter->first == name && iter->second == dir) {
      m_recent_sessions.erase(iter);
      old = true;
      break;
    }
  }
  
  if (!old) {
    MenuItem* mi = manage(new MenuItem(name));
    mi->signal_activate().connect(bind(signal_restore_session, dir));
    m_restorerecentmenu.append(*mi);
    m_restorerecentmenu.show_all();
  }

  m_recent_sessions.push_back(make_pair(name, dir));
  dump_recent();
}


void PopupMenu::do_set_session_dir() {
  struct stat stats;
  if (m_session_save_dialog.run() == RESPONSE_OK) {
    if ((stat(m_session_save_dialog.get_filename().c_str(), &stats) == -1) 
        && (errno == ENOENT)) {
      signal_set_session_dir(m_session_save_dialog.get_filename());
    }
    else {
      // TODO: this case will never happen as the filechooser won't let us 
      // choose existing directories. it rather enters them upon pressing
      // return.
      MessageDialog dialog("Couldn't move session directory. "
                           "Directory exists."); 
      dialog.run();
    }
  }
  m_session_save_dialog.hide();
}


void PopupMenu::do_set_session_name() {
  Gtk::Dialog dialog("Enter new session name");

  dialog.add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);
  dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  dialog.set_default_response(Gtk::RESPONSE_OK);
  Gtk::Entry entry;
  dialog.get_vbox()->pack_end(entry);
  entry.show();
  entry.set_text(m_session_name);
  entry.signal_activate().
    connect(bind<int>(mem_fun(dialog, &Gtk::Dialog::response), 
                      Gtk::RESPONSE_OK));
  if ((dialog.run() == Gtk::RESPONSE_OK) && (entry.get_text() != ""))
    signal_set_session_name(entry.get_text());
}


void PopupMenu::do_restore() {
  if (m_session_dialog.run() == RESPONSE_OK) {
    signal_restore_session(m_session_dialog.get_filename());
    m_session_dialog.set_filename(m_session_dialog.get_current_folder());
  }
  m_session_dialog.hide();
}


void PopupMenu::do_quit() {
  if (m_lashd_running) {
    MessageDialog dlg("Do you want to shut down the session handler?",
                      false, MESSAGE_QUESTION, BUTTONS_YES_NO);
    if (dlg.run() == RESPONSE_YES) {
      internal_signal_lashd_stopped.connect(&Main::quit);
      signal_stop_lashd();
    }
    else
      Main::quit();
  }
  else
    Main::quit();
}


void PopupMenu::add_input(const std::string& name, const std::string& type) {
  m_porttype[name] = type;
  m_inputs.insert(name);
  std::map<string, Gtk::MenuItem*>::iterator iter;
  for (iter = m_outputs.begin(); iter != m_outputs.end(); ++iter) {
    if (m_porttype[iter->first] == type) {
      CheckMenuItem* mi = manage(new CheckMenuItem(name));
      iter->second->get_submenu()->append(*mi);
      m_inputitems[iter->second][name] = mi;
      iter->second->get_submenu()->show_all();
      mi->signal_activate().
        connect(bind(bind(mem_fun(*this, &PopupMenu::toggle_connection), name),
                     iter->first));
    }
  }
}


void PopupMenu::add_output(const std::string& name, const std::string& type) {
  m_porttype[name] = type;
  ImageMenuItem* mi = manage(new ImageMenuItem(name));
  if (type == "8 bit raw midi")
    mi->set_image(*manage(new Gtk::Image(m_midi_pixbuf)));
  else if (type == JACK_DEFAULT_AUDIO_TYPE)
    mi->set_image(*manage(new Gtk::Image(m_audio_pixbuf)));
  Menu* m = manage(new Menu);
  std::set<string>::const_iterator siter;
  for (siter = m_inputs.begin(); siter != m_inputs.end(); ++siter) {
    if (m_porttype[*siter] == type) {
      CheckMenuItem* mi2 = manage(new CheckMenuItem(*siter));
      mi2->signal_activate().
        connect(bind(bind(mem_fun(*this, &PopupMenu::toggle_connection), *siter),
                     name));
      m_inputitems[mi][*siter] = mi2;
      m->append(*mi2);
    }
  }
  mi->set_submenu(*m);
  m_connect_menu.append(*mi);
  m_outputs[name] = mi;
  m_connect_menu.show_all();
}


void PopupMenu::remove_input(const std::string& name) {
  std::map<string, string>::iterator titer = m_porttype.find(name);
  m_porttype.erase(titer);
  
  std::map<MenuItem*, std::map<string, CheckMenuItem*> >::iterator iter;
  for (iter = m_inputitems.begin(); iter != m_inputitems.end(); ++iter) {
    std::map<string, CheckMenuItem*>::iterator iter2 = iter->second.find(name);
    if (iter2 != iter->second.end()) {
      iter->first->get_submenu()->remove(*iter2->second);
      iter->second.erase(iter2);
    }
  }
  std::set<string>::iterator iter3 = m_inputs.find(name);
  if (iter3 != m_inputs.end())
    m_inputs.erase(iter3);
}


void PopupMenu::remove_output(const std::string& name) {
  std::map<string, string>::iterator titer = m_porttype.find(name);
  m_porttype.erase(titer);

  std::map<string, MenuItem*>::iterator iter = m_outputs.find(name);
  if (iter != m_outputs.end()) {
    std::map<MenuItem*, std::map<string, CheckMenuItem*> >::iterator iter2;
    iter2 = m_inputitems.find(m_outputs[name]);
    if (iter2 != m_inputitems.end())
      m_inputitems.erase(iter2);
    m_connect_menu.items().remove(*m_outputs[name]);
    m_outputs.erase(iter);
  }
}


void PopupMenu::toggle_connection(const std::string& output, 
                                 const std::string& input) {
  std::map<string, MenuItem*>::const_iterator iter = m_outputs.find(output);
  if (iter != m_outputs.end()) {
    std::map<string, CheckMenuItem*>::const_iterator iter2;
    iter2 = m_inputitems[iter->second].find(input);
    if (iter2 != m_inputitems[iter->second].end()) {
      CheckMenuItem* cmi = iter2->second;
      if (!cmi->get_active())
        signal_disconnect(output, input);
      else
        signal_connect(output, input);
    }
  }
}


void PopupMenu::connect(const std::string& output, const std::string& input) {
  std::map<string, MenuItem*>::iterator oiter = m_outputs.find(output);
  if (oiter != m_outputs.end()) {
    std::map<MenuItem*, std::map<string, CheckMenuItem*> >::iterator iter;
    if ((iter = m_inputitems.find(oiter->second)) != m_inputitems.end()) {
      std::map<string, CheckMenuItem*>::iterator iter2;
      if ((iter2 = iter->second.find(input)) != iter->second.end())
        iter2->second->set_active(true);
    }
  }
}


void PopupMenu::disconnect(const std::string& output, const std::string& input) {
  std::map<string, MenuItem*>::iterator oiter = m_outputs.find(output);
  if (oiter != m_outputs.end()) {
    std::map<MenuItem*, std::map<string, CheckMenuItem*> >::iterator iter;
    if ((iter = m_inputitems.find(oiter->second)) != m_inputitems.end()) {
      std::map<string, CheckMenuItem*>::iterator iter2;
      if ((iter2 = iter->second.find(input)) != iter->second.end())
        iter2->second->set_active(false);
    }
  }
}


void PopupMenu::dump_recent() {
  ofstream ofs((Glib::getenv("HOME") + "/.glashctl_recent").c_str());
  size_t i = m_recent_sessions.size() - 10;
  i = (i < 0 ? 0 : i);
  for ( ; i < m_recent_sessions.size(); ++i)
    ofs<<m_recent_sessions[i].first<<endl<<m_recent_sessions[i].second<<endl;
}

