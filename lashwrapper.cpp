/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <glibmm.h>

#include "lashwrapper.hpp"
#include <signal.h>


using namespace Glib;
using namespace std;
using namespace sigc;


LASHWrapper::LASHWrapper(int* argc, char*** argv) 
  : m_running(0),
    m_old_running(0), 
    m_send_delayed_start(false), 
    m_lsi(0) {
  
  m_args = lash_extract_args(argc, argv);
  
  // check if lashd is already running
  if ((m_lsi = create_server_interface(0))) {
    m_send_delayed_start = true;
  }
  
  // check if lashd has started or stopped a couple of times every second
  signal_timeout().connect(mem_fun(*this, &LASHWrapper::check_running), 100);
}


void LASHWrapper::start_lashd() {

  // using the same double fork() trick as JACK does to prevent
  // zombie children
  int err = fork();
  
  // child process will run this statement
  if (err == 0) {
    switch (fork()) {
      
    // grandchild process will run this block
    case 0:
      setsid();
      execlp("lashd", "lashd", NULL);
      _exit(-1);
      
    // this block only runs if the second fork() fails
    case -1:
      _exit(-1);
      
    // exit the child process here to force the grandchild to become a child
    // of init
    default: 
      _exit(0);
    }
  }
  // if the fork succeeded, waitpid for the child
  else if (err > 0) {
    waitpid(err, NULL, 0);
    ++m_running;
  }
  
  // XXX else, print error message
}


void LASHWrapper::stop_lashd() {
  
  /* This is how it works:
     - if there is no active session, just kill lashd
     - if there are active sessions, tell lashd to close them and wait
       until they are all closed, then kill lashd
  */

  if (!m_lsi || !m_lsi->has_session())
    really_stop_lashd();
  else {
    m_lsi->signal_session_changed.
      connect(mem_fun(*this, &LASHWrapper::wait_for_sessions));
    m_lsi->close_all_sessions();
  }
}


void LASHWrapper::restore_session(const string& dir) {
  if (m_lsi)
    m_lsi->restore_session(dir);
}


void LASHWrapper::set_session_dir(const string& dir) {
  if (m_lsi)
    m_lsi->set_session_dir(dir);
} 


void LASHWrapper::set_session_name(const string &name) {
  if (m_lsi)
    m_lsi->rename_session(name);
}


void LASHWrapper::save_session() {
  if (m_lsi)
    m_lsi->save_session();
}


void LASHWrapper::close_session() {
  if (m_lsi)
    m_lsi->close_session();
}


bool LASHWrapper::check_running() {

  if (m_send_delayed_start) {
    m_send_delayed_start = false;
    signal_lashd_started();
  }
  else if (m_lsi && !m_lsi->is_valid()) {
    signal_lashd_stopped();
    delete m_lsi;
    m_lsi = 0;
  }
  else if (m_running != m_old_running) {
    m_old_running = m_running;
    if ((m_lsi = create_server_interface(5)))
      signal_lashd_started();
  }
  
  return true;
}


LASHServerInterface* LASHWrapper::create_server_interface(int timeout) {
  assert(m_lsi == 0);
  m_lsi = new LASHServerInterface(m_args, "glashctl", timeout);
  if (!m_lsi->is_valid()) {
    delete m_lsi;
    m_lsi = 0;
  }
  else {
    m_lsi->signal_session_changed.connect(signal_session_changed);
    m_lsi->signal_event_received.connect(signal_event_received);
    m_lsi->signal_session_saved.connect(signal_session_saved);
  }
  return m_lsi;
}


void LASHWrapper::wait_for_sessions(const string& session) {
  /* If the session name is "" it means that there is no active session,
     so it's time to kill lashd. */
  if (session == "")
    really_stop_lashd();
}


void LASHWrapper::really_stop_lashd() {
  system("pkill lashd");
}
