/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#ifndef LASHSERVERINTERFACE_HPP
#define LASHSERVERINTERFACE_HPP

#include <list>
#include <string>

#include <sigc++/sigc++.h>
#include <lash/lash.h>


/** This class wraps parts of the LASH server interface API in a
    convenient class. */
class LASHServerInterface : public sigc::trackable {
public:
  
  /** This constructor creates a new LASHServerInterface object and tries
      to connect to the LASH server. @c classname is the application class
      name that will be used when connecting, and @c timeout is the number
      of seconds it should try to connect before giving up .*/
  LASHServerInterface(lash_args_t* args, const std::string& classname, 
                      int timeout);
  ~LASHServerInterface();
  
  /** This returns @c true if the LASH client was initialised OK and still is
      connected to the LASH server. */
  bool is_valid() const;
  
  /** This returns @c true if there is an active session. */
  bool has_session() const;
  
  /** This tells the server to restore the session in the given directory. */
  void restore_session(const std::string& directory);

  /** This tells the server to set the directory for this session */
  void set_session_dir(const std::string& directory);
  
  /** This tells the server to save the current session. */
  void save_session();
  
  /** This tells the server to close the current session. */
  void close_session();
  
  /** This tells the server to close all open sessions. */
  void close_all_sessions();
  
  /** This tells the server to rename the current session. */
  void rename_session(const std::string& new_name);

  
  /** This signal is emitted when the active session changes, or its name
      changes. */
  sigc::signal<void, const std::string&> signal_session_changed;
  
  /** This signal is emitted when a session has been saved. The first
      parameter is the session name and the second is the directory it
      was saved in. */
  sigc::signal<void, const std::string&, const std::string&> 
  signal_session_saved;
  
  /** This signal is emitted when an event is received. */
  sigc::signal<void, const std::string&> signal_event_received;
  
private:
  
  struct Project {
    std::string name;
    std::string dir;
  };
  
  bool check_events();
  
  std::list<Project> m_projects;
  
  sigc::connection m_connection;
  lash_client_t* m_client;
};


#endif
