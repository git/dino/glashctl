/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#ifndef TRAYICON_HPP
#define TRAYICON_HPP

#include <string>
#include <map>
#include <set>

#include <gtkmm/eventbox.h>
#include <gtkmm/iconset.h>
#include <gtkmm/image.h>
#include <gtkmm/menu.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/statusicon.h>

#include "sessionchooserdialog.hpp"


/** The main tray icon for GLASHCtl. All operations are done from its popup
    menu. */
class TrayIcon : public Gtk::StatusIcon {
public:

  TrayIcon();
  
  /** Called when the LASH daemon has been started. */
  void lashd_started();
  
  /** Called when the LASH daemon has been stopped. */
  void lashd_stopped();
  
  /** Emitted when the user right-clicks the icon. */
  sigc::signal<void, guint, guint> signal_popup;
  
private:
  
  static void popup_menu(GtkStatusIcon* obj, guint button, 
                         guint activate_time, gpointer data);
  
  Glib::RefPtr<Gdk::Pixbuf> m_active_pixbuf;
  Glib::RefPtr<Gdk::Pixbuf> m_inactive_pixbuf;

};


#endif
