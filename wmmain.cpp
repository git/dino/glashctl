/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <iostream>
#include <csignal>

#include <gtk/gtk.h>
#include <gtkmm.h>

#include "jackwrapper.hpp"
#include "lashwrapper.hpp"
#include "popupmenu.hpp"
#include "wmdockicon.hpp"


using namespace std;
using namespace Gtk;
using namespace Gdk;
using namespace Glib;


int die_now = 0;


void signalhandler(int signal) {
  die_now = 1;
}


bool check_death() {
  if (die_now == 1)
    Main::quit();
  return true;
}


int main(int argc, char** argv) {
  
  // exit cleanly when interrupted by the user
  std::signal(SIGHUP, &signalhandler);
  std::signal(SIGINT, &signalhandler);
  std::signal(SIGTERM, &signalhandler);
  
  // create the objects
  Main kit(argc, argv);
  LASHWrapper lashd(&argc, &argv);
  JACKWrapper jackd("glashctl");
  PopupMenu menu;
  WMDockIcon icon;
  
  // connect the signals
  menu.signal_start_lashd.connect(mem_fun(lashd, &LASHWrapper::start_lashd));
  menu.signal_stop_lashd.connect(mem_fun(lashd, &LASHWrapper::stop_lashd));
  menu.signal_close_session.connect(mem_fun(lashd,&LASHWrapper::close_session));
  menu.signal_save_session.connect(mem_fun(lashd, &LASHWrapper::save_session));
  menu.signal_restore_session.
    connect(mem_fun(lashd, &LASHWrapper::restore_session));
  menu.signal_set_session_dir.
    connect(mem_fun(lashd, &LASHWrapper::set_session_dir));
  menu.signal_set_session_name.
    connect(mem_fun(lashd, &LASHWrapper::set_session_name));
  menu.signal_connect.connect(mem_fun(jackd, &JACKWrapper::connect));
  menu.signal_disconnect.connect(mem_fun(jackd, &JACKWrapper::disconnect));

  lashd.signal_lashd_started.connect(mem_fun(icon, &WMDockIcon::lashd_started));
  lashd.signal_lashd_stopped.connect(mem_fun(icon, &WMDockIcon::lashd_stopped));
  lashd.signal_lashd_started.connect(mem_fun(menu, &PopupMenu::lashd_started));
  lashd.signal_lashd_stopped.connect(mem_fun(menu, &PopupMenu::lashd_stopped));
  lashd.signal_session_changed.
    connect(mem_fun(menu, &PopupMenu::session_changed));
  lashd.signal_session_saved.connect(mem_fun(menu, &PopupMenu::session_saved));

  jackd.signal_input_added.connect(mem_fun(menu, &PopupMenu::add_input));
  jackd.signal_input_removed.connect(mem_fun(menu, &PopupMenu::remove_input));
  jackd.signal_output_added.connect(mem_fun(menu, &PopupMenu::add_output));
  jackd.signal_output_removed.connect(mem_fun(menu, &PopupMenu::remove_output));
  jackd.signal_connected.connect(mem_fun(menu, &PopupMenu::connect));
  jackd.signal_disconnected.connect(mem_fun(menu, &PopupMenu::disconnect));
  jackd.force_check();
  
  typedef void (Gtk::Menu::*popup_fun_t)(guint, guint);
  icon.signal_popup.connect(mem_fun(menu, (popup_fun_t)&PopupMenu::popup));
  
  signal_timeout().connect(&check_death, 100);
  
  // run
  kit.run();

  return 0;
}

