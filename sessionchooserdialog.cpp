/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <iostream>

#include <gtk/gtktreeitem.h>
#include <gtk/gtktreeview.h>
#include <glibmm/fileutils.h>

#include "sessionchooserdialog.hpp"


using namespace std;
using namespace Gtk;


SessionChooserDialog::SessionChooserDialog()
  : FileChooserDialog("Select session", FILE_CHOOSER_ACTION_SELECT_FOLDER) {
  
  signal_current_folder_changed().
    connect(mem_fun(*this, &SessionChooserDialog::current_folder_changed));
}


void SessionChooserDialog::current_folder_changed() {
  string dirname = get_current_folder();
  Glib::Dir dir(get_current_folder());
  for (Glib::DirIterator iter = dir.begin(); iter != dir.end(); ++iter) {
    if (*iter == ".lash_info") {
      response(RESPONSE_OK);
      return;
    }
  }
}

