/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#ifndef POPUPMENU_HPP
#define POPUPMENU_HPP

#include <map>
#include <set>
#include <string>
#include <vector>

#include <gtkmm.h>

#include "sessionchooserdialog.hpp"


class PopupMenu : public Gtk::Menu {
public:
  
  PopupMenu();
  
  /** Called when the LASH daemon has been started. */
  void lashd_started();
  
  /** Called when the LASH daemon has been stopped. */
  void lashd_stopped();

  /** Called when the active LASH session's name has changed. */
  void session_changed(const std::string& name);
  
  /** Called when the active LASH session has been saved. */
  void session_saved(const std::string& name, const std::string& dir);
  
  /** Called when a new JACK input port has appeared. */
  void add_input(const std::string& name, const std::string& type);
  
  /** Called when a new JACK output port has appeared. */
  void add_output(const std::string& name, const std::string& type);
  
  /** Called when a JACK input port has disappeared. */
  void remove_input(const std::string& name);
  
  /** Called when a JACK output port has disappeared. */
  void remove_output(const std::string& name);
  
  /** Called when two JACK ports have been connected. */
  void connect(const std::string& output, const std::string& input);
  
  /** Called when two JACK ports have been disconnected. */
  void disconnect(const std::string& output, const std::string& input);
  
  /** Emitted when the TrayIcon wants to start lashd. */
  sigc::signal<void> signal_start_lashd;

  /** Emitted when the TrayIcon wants to stop lashd. */
  sigc::signal<void> signal_stop_lashd;

  /** Emitted when the TrayIcon wants to save the active LASH session. */
  sigc::signal<void> signal_save_session;

  /** Emitted when the TrayIcon wants to move the active LASH session. */
  sigc::signal<void, const std::string&> signal_set_session_dir;

  /** Emitted when the TrayIcon wants to rename the active LASH session. */
  sigc::signal<void, const std::string&> signal_set_session_name;

  /** Emitted when the TrayIcon wants to close the active LASH session. */
  sigc::signal<void> signal_close_session;

  /** Emitted when the TrayIcon wants to restore a LASH session. */
  sigc::signal<void, const std::string&> signal_restore_session;
  
  /** Emitted when the TrayIcon wants to connect two JACK ports. */
  sigc::signal<void, const std::string&, const std::string&> signal_connect;

  /** Emitted when the TrayIcon wants to disconnect two JACK ports. */
  sigc::signal<void, const std::string&, const std::string&> signal_disconnect;
  
protected:

  void do_restore();
  void do_quit();
  void do_set_session_name();
  void do_set_session_dir();

  void toggle_connection(const std::string& output, const std::string& input);
  
  void dump_recent();
  
  std::string m_session_name;
  
  Glib::RefPtr<Gdk::Pixbuf> m_midi_pixbuf;
  Glib::RefPtr<Gdk::Pixbuf> m_audio_pixbuf;

  Gtk::Menu m_restorerecentmenu;
  Gtk::Menu m_connect_menu;
  
  Gtk::MenuItem m_connectitem;
  Gtk::MenuItem m_startitem;
  Gtk::MenuItem m_stopitem;
  Gtk::MenuItem m_restoreitem;
  Gtk::MenuItem m_restorerecentitem;
  Gtk::MenuItem m_saveitem;
  Gtk::MenuItem m_closeitem;
  Gtk::MenuItem m_setsessiondiritem;
  Gtk::MenuItem m_setsessionnameitem;
  
  SessionChooserDialog m_session_dialog;
  Gtk::FileChooserDialog m_session_save_dialog;

  bool m_lashd_running;
  sigc::signal<void> internal_signal_lashd_stopped;
  
  std::map<std::string, Gtk::MenuItem*> m_outputs;
  std::set<std::string> m_inputs;
  std::map<Gtk::MenuItem*, std::map<std::string, Gtk::CheckMenuItem*> > 
  m_inputitems;
  std::map<std::string, std::string> m_porttype;
  std::vector<std::pair<std::string, std::string> > m_recent_sessions;
  
};


#endif
