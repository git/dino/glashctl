/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#include <iostream>

#include <glibmm.h>

#include "jackwrapper.hpp"


using namespace std;
using namespace Glib;
using namespace sigc;


JACKWrapper::JACKWrapper(const std::string& client_name) 
  : m_client(0) {

  if ((m_client = jack_client_open(client_name.c_str(), JackNullOption, 0))) {
    sem_init(&m_portreg_sem, 0, 0);
    signal_timeout().connect(mem_fun(*this, &JACKWrapper::check_portreg), 100);
    jack_set_port_registration_callback(m_client, 
                                        &JACKWrapper::port_registration_callback,
                                        this);
    jack_set_graph_order_callback(m_client, 
                                  &JACKWrapper::graph_order_callback, this);
    jack_activate(m_client);
    
    signal_timeout().connect(mem_fun(*this, &JACKWrapper::force_check), 4000); 
  }
  
}


JACKWrapper::~JACKWrapper() {
  if (m_client) {
    jack_deactivate(m_client);
    jack_client_close(m_client);
    sem_destroy(&m_portreg_sem);
  }
}


void JACKWrapper::connect(const std::string& output, const std::string& input) {
  if (m_client)
    jack_connect(m_client, output.c_str(), input.c_str());
}


void JACKWrapper::disconnect(const std::string& output, 
                             const std::string& input) {
  if (m_client)
    jack_disconnect(m_client, output.c_str(), input.c_str());
}


bool JACKWrapper::force_check() {
  if (m_client)
    sem_post(&m_portreg_sem);
  return true;
}


void JACKWrapper::port_registration_callback(jack_port_id_t, int, void* arg) {
  sem_post(&static_cast<JACKWrapper*>(arg)->m_portreg_sem);
}


int JACKWrapper::graph_order_callback(void* arg) {
  sem_post(&static_cast<JACKWrapper*>(arg)->m_portreg_sem);
  return 0;
}


bool JACKWrapper::check_portreg() {
  if (!sem_trywait(&m_portreg_sem)) {
    
    const char** ports = jack_get_ports(m_client, 0, 0, 0);
    if (ports) {
      
      // check for new or removed ports
      
      // mark all old ones as gone
      map<string, PortInfo>::iterator iter;
      for (iter = m_ports.begin(); iter != m_ports.end(); ++iter)
        iter->second.flag = false;
      
      // add new ports to our map and mark old ones as still here
      for (int i = 0; ports[i]; ++i) {
        jack_port_t* port = jack_port_by_name(m_client, ports[i]);
        PortInfo* pi = 0;
        if (m_ports.find(ports[i]) == m_ports.end()) {
          if (port) {
            m_ports[ports[i]].type = jack_port_type(port);
            if (jack_port_flags(port) & JackPortIsInput) {
              signal_input_added(ports[i], m_ports[ports[i]].type);
              m_ports[ports[i]].is_input = true;
            }
            else {
              signal_output_added(ports[i], m_ports[ports[i]].type);
              m_ports[ports[i]].is_input = false;
            }
          }
        }
        pi = &m_ports[ports[i]];
        pi->flag = true;
      }
      
      // remove unregistered ports from our map
      for (iter = m_ports.begin(); iter != m_ports.end(); ) {
        if (iter->second.flag == false) {
          map<string, PortInfo>::iterator iter2 = iter;
          ++iter;
          if (iter2->second.is_input)
            signal_input_removed(iter2->first);
          else
            signal_output_removed(iter2->first);
          m_ports.erase(iter2);
        }
        else
          ++iter;
      }
      
      
      free(ports);
    }


    // check for new or removed connections for all output ports
    
    map<string, PortInfo>::iterator piter;
    for (piter = m_ports.begin(); piter != m_ports.end(); ++piter) {
      if (!piter->second.is_input) {
        
        jack_port_t* port = jack_port_by_name(m_client, piter->first.c_str());
        if (port) {
        
          const char** connections = jack_port_get_all_connections(m_client,
                                                                   port);
          if (connections) {
            
            // mark all connections as gone
            map<string, bool>::iterator pciter;
            for (pciter = piter->second.connections.begin();
                 pciter != piter->second.connections.end(); ++pciter)
              pciter->second = false;
            
            // find new connections and mark old ones that are still here
            for (int c = 0; connections[c]; ++c) {
              if (piter->second.connections.find(connections[c]) ==
                  piter->second.connections.end()) {
                signal_connected(piter->first, connections[c]);
              }
              piter->second.connections[connections[c]] = true;
            }
            
            // remove all connections that are not here anymore
            for (pciter = piter->second.connections.begin();
                 pciter != piter->second.connections.end(); ) {
              if (pciter->second == false) {
                map<string, bool>::iterator tmp = pciter;
                ++pciter;
                signal_disconnected(piter->first, tmp->first);
                piter->second.connections.erase(tmp);
              }
              else
                ++pciter;
            }

            free(connections);
          }
          
          // if there are no connections, clear all old ones
          else {
            map<string, bool>::iterator pciter;
            for (pciter = piter->second.connections.begin();
                 pciter != piter->second.connections.end(); ++pciter)
              signal_disconnected(piter->first, pciter->first);
            piter->second.connections.clear();
          }
          
        }
      }
    }

  }
  
  return true;
}



