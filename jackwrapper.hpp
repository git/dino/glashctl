/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006  Lars Luthman <lars.luthman@gmail.com>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#ifndef JACKWRAPPER_HPP
#define JACKWRAPPER_HPP

#include <map>
#include <set>
#include <string>

#include <semaphore.h>

#include <sigc++/sigc++.h>
#include <jack/jack.h>


/** A class that wraps the port handling functions in libjack. */
class JACKWrapper : public sigc::trackable {
public:
  
  /** Create a JACKWrapper object with the JACK client name @c client_name. */
  JACKWrapper(const std::string& client_name);
  
  ~JACKWrapper();
  
  
  /** Connect two JACK ports. */
  void connect(const std::string& output, const std::string& input);

  /** Disconnect two JACK ports. */
  void disconnect(const std::string& output, const std::string& input);
  
  /** Check for new ports and connections. You should only need to call this
      right after creating the JACKWrapper, after that the checks should be
      automatic. */
  bool force_check();
  
  /** Emitted when an input port has been added. */
  sigc::signal<void, const std::string&, const std::string&> signal_input_added;

  /** Emitted when an input port has been removed. */
  sigc::signal<void, const std::string&> signal_input_removed;

  /** Emitted when an output port has been added. */
  sigc::signal<void, const std::string&,const std::string&>signal_output_added;

  /** Emitted when an output port has been removed. */
  sigc::signal<void, const std::string&> signal_output_removed;

  /** Emitted when two ports have been connected. */
  sigc::signal<void, const std::string&, const std::string&> signal_connected;

  /** Emitted when two ports have been disconnected. */
  sigc::signal<void, const std::string&,const std::string&> signal_disconnected;
  
protected:
  
  static void port_registration_callback(jack_port_id_t, int, void *arg);
  static int graph_order_callback(void* arg);

  bool check_portreg();
  
  jack_client_t* m_client;
  sem_t m_portreg_sem;
  
  struct PortInfo {
    std::string type;
    bool is_input;
    bool flag;
    std::map<std::string, bool> connections;
  };
  
  std::map<std::string, PortInfo> m_ports;
};


#endif
