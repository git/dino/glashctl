// -*- Mode: C++ ; c-basic-offset: 2 -*-
/****************************************************************************
   GLASHCtl - a simple tray applet for controlling lashd
   
   Copyright (C) 2006 Nedko Arnaudov <nedko@arnaudov.name>
   Modified by Lars Luthman (blame all bugs on me!)
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 3 of the License.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation, 
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
****************************************************************************/

#ifndef WMDOCKICON_HPP__BFA76043_95E1_4CDC_BFE6_7FF1A53BFB23__INCLUDED
#define WMDOCKICON_HPP__BFA76043_95E1_4CDC_BFE6_7FF1A53BFB23__INCLUDED

#include <sigc++/signal.h>
#include <glibmm/refptr.h>
#include <gdkmm/window.h>


/** A class that creates a WindowMaker compatible DockApp window
    with a LASH icon. */
class WMDockIcon : public sigc::trackable {
public:
  
  WMDockIcon();
  
  /** Called when the LASH daemon has been started. */
  void lashd_started();
  
  /** Called when the LASH daemon has been stopped. */
  void lashd_stopped();
  
  /** Emitted when the user right-clicks the icon. */
  sigc::signal<void, guint, guint> signal_popup;
  
private:
  
  static void button_pressed(GdkEvent* event, gpointer data);

  Glib::RefPtr<Gdk::Window> m_win;
  Glib::RefPtr<Gdk::Window> m_iconwin;
  Glib::RefPtr<Gdk::Pixmap> m_active_pixmap;
  Glib::RefPtr<Gdk::Pixmap> m_inactive_pixmap;
};


#endif // #ifndef WMDOCKICON_HPP__BFA76043_95E1_4CDC_BFE6_7FF1A53BFB23__INCLUDED
